from functions import m_json 
from functions import m_pck

#from m_json import * 
#from m_pck import *

# Data path zu setup datei festlegen
path = '/home/pi/calorimetry_home/datasheets/setup_newton.json'

# metadata auslesen
metadata = m_json.get_metadata_from_setup(path)

# serials hinzufügen
metadata = m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', metadata)

# Messung durchführen und Temperaturen und Zeiten abspeichern
data = m_pck.get_meas_data_calorimetry(metadata)

#H5 Datei erstellen
m_pck.logging_calorimetry(data, metadata,'/home/pi/calorimetry_home/data','/home/pi/calorimetry_home/datasheets')

#setup archivieren
m_json.archiv_json('/home/pi/calorimetry_home',path, '/home/pi/calorimetry_home/data/archiv')









